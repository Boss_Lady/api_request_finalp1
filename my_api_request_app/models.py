from django.db import models
from django.contrib.auth.models import User
from my_api_request_app.views import get_status_description, CSR

class CSR(models.Model):
    ENV_CHOICES = (
        ('prod', 'Prod'),
        ('dev', 'Dev'),
        ('test', 'Test'),
    )

    request_id = models.BigAutoField(primary_key=True, verbose_name='ID')
    title = models.CharField(max_length=100)
    description = models.TextField()
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='csrs')
    created_at = models.DateTimeField(auto_now_add=True)
    link = models.URLField(max_length=200)
    environment = models.CharField(max_length=10, choices=ENV_CHOICES)

    def __str__(self):
        return self.title


class StatusReport(models.Model):
    link = models.URLField(max_length=200)
    status_code = models.IntegerField()
    status_description = models.CharField(max_length=200)
    is_working = models.BooleanField()
    created_at = models.DateTimeField(auto_now_add=True)
    username = models.CharField(max_length=150)
    environment = models.CharField(max_length=10, choices=CSR.ENV_CHOICES)

    def save_status_report(self, link, status_code, environment, user):
        status_description = get_status_description(status_code)
        is_working = status_code == 200
        username = user.username if user.is_authenticated else "Anonymous User"

        self.link = link
        self.status_code = status_code
        self.status_description = status_description
        self.is_working = is_working
        self.username = username
        self.environment = environment
        self.save()

    def __str__(self):
        return f"Status Report for {self.link}"
