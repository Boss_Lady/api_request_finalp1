from datetime import datetime
import requests
from django.shortcuts import render, redirect
from django.http import HttpResponse
from my_api_request_app.models import CSR
from my_api_request_app.forms import CSRForm
from django.views.decorators.csrf import csrf_exempt

@csrf_exempt
def submit_CSR(request):
    if request.method == 'POST':
        title = request.POST.get('title')
        description = request.POST.get('description')
        user = request.POST.get('user')
        link = request.POST.get('link')
        environment = request.POST.get('env')

        if not link:
            return HttpResponse("Link is required", status=400)

        return test_link(request, link, environment)
    
    
@csrf_exempt
def test_link(request, link, environment):
    created_at = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    username = request.user.username if request.user.is_authenticated else "Anonymous User"

    return render(request, 'submit_CSR.html', {'link': link, 'created_at': created_at, 'username': username, 'environment': environment})    

def get_status_description(status_code):
    status_codes = {
        200: "OK - The request was successful",
        400: "Bad Request - The server could not understand the request",
        401: "Unauthorized - The requested resource requires authentication",
        403: "Forbidden - Access to the requested resource is forbidden",
        404: "Not Found - The requested resource was not found on the server",
        500: "Internal Server Error - The server encountered an unexpected condition",
        503: "Service Unavailable - The server is currently unavailable",
    }
    return status_codes.get(status_code, "Unknown Error")

def submit_CSR(request):
    if request.method == 'POST':
        link = request.POST.get('link')
        environment = request.POST.get('env')

        try:
            response = requests.get(link)
            status_code = response.status_code
            status_description = get_status_description(status_code)
            is_working = status_code == 200
        except requests.exceptions.RequestException as e:
            status_code = "Error"
            status_description = str(e)
            is_working = False

        created_at = datetime.now()
        username = request.user.username if request.user.is_authenticated else "John Doe"

        return render(request, 'my_api_request_app/status_report.html', {'link': link, 'status_code': status_code, 'status_description': status_description, 'is_working': is_working, 'created_at': created_at, 'username': username, 'environment': environment})

    return HttpResponse("Method Not Allowed", status=405)

def status_report(request):
    link = request.POST.get('link')
    environment = request.POST.get('env')

    status_report_data = submit_CSR(request)

    return render(request, 'my_api_request_app/status_report.html', {'status_report_data': status_report_data})

def show_CSR(request, id):
    try:
        csr = CSR.objects.get(id=id)
    except CSR.DoesNotExist:
        return HttpResponse("CSR not found", status=404)

    context = {
        'csr': csr,
    }
    return render(request, 'my_api_request_app/detail.html', context)

def list_CSR(request):
    csr = CSR.objects.all()
    context = {
        'csr': csr,
    }
    return render(request, 'my_api_request_app/list.html', context)

def create_CSR(request):
    if request.method == 'POST':
        csr_form = CSRForm(request.POST)
        if csr_form.is_valid():
            new_csr = csr_form.save(commit=False)
            new_csr.environment = request.POST.get('environment')
            new_csr.save()
            return redirect('list_CSR')
    else:
        csr_form = CSRForm()

    context = {
        'form': csr_form,
    }
    return render(request, 'my_api_request_app/create.html', context)

def search_CSR(request):
    if request.method == 'POST':
        search_query = request.POST.get('search', '')
        csr = CSR.objects.filter(request_id__icontains=search_query) | CSR.objects.filter(user__username__icontains=search_query)
        return render(request, 'my_api_request_app/search_results.html', {'csr': csr, 'search_query': search_query})
    else:
        return render(request, 'my_api_request_app/search_form.html')

def main_view(request):
    return render(request, 'detail.html')

def list_CSR(request):
    return render(request, 'list_CSR.html')

def create_CSR(request):
    return render(request, 'create.html')

def show_CSR(request, id):
    # Logic to fetch CSR item with id and pass it to the template
    return render(request, 'detail.html', {'id': id})

def search_CSR(request):
    return render(request, 'search_form.html')

def status_report(request):
    return render(request, 'status_report.html')

def submit_CSR(request):
    return render(request, 'submit_CSR.html')

