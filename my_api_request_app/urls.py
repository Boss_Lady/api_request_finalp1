from django.urls import path
from . import views

urlpatterns = [
    path('', views.main_view, name='main_view'),
    path('list_CSR/', views.list_CSR, name='list_csr'),
    path('create_CSR/', views.create_CSR, name='create_csr'),
    path('csr/show/<int:id>/', views.show_CSR, name='show_CSR'),
    path('csr/search/', views.search_CSR, name='search_CSR'),
    path('status_report/', views.status_report, name='status_report'),
    path('csr/submit/', views.submit_CSR, name='submit_CSR'),  # Updated URL mapping for submit_CSR view
]
# urls.py


