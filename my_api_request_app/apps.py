from django.apps import AppConfig

class MyApiRequestAppConfig(AppConfig):
        default_auto_field = 'django.db.models.BigAutoField'
        name = 'my_api_request_app'
    
