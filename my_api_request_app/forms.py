from django.forms import ModelForm
from my_api_request_app.models import CSR
from django import forms

class CSRForm(forms.ModelForm):
    class Meta:
        model = CSR
        fields = '__all__'
