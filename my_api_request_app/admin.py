from django.contrib import admin
from django.utils.html import format_html
from django.urls import reverse
from .models import CSR, StatusReport

@admin.register(CSR)
class CSRAdmin(admin.ModelAdmin):
    list_display = ['request_id', 'title', 'description', 'get_username', 'get_created_at', 'link']

    def get_username(self, obj):
        username = obj.user.username if obj.user else None
        return username

    def get_created_at(self, obj):
        return obj.created_at

    def link(self, obj):
        if obj.pk:
            edit_url = reverse('admin:%s_%s_change' % (obj._meta.app_label, obj._meta.model_name), args=[obj.pk])
            return format_html('<a href="{}">{}</a>'.format(edit_url, obj.link))
        return None

    get_username.short_description = 'Username'
    get_created_at.short_description = 'Created At'
    link.short_description = 'Link'

    ordering = ['title']

@admin.register(StatusReport)
class StatusReportAdmin(admin.ModelAdmin):
    list_display = ['link', 'status_code', 'status_description', 'is_working', 'created_at', 'username', 'environment']
    ordering = ['created_at']

# Register your models here.
