from django.http import HttpResponse
from django.shortcuts import render
from datetime import datetime
from django.views.decorators.csrf import csrf_exempt
from my_api_request_app import get_status_description
import requests

# Moved get_status_description function to a separate module (my_api_request_app) to avoid conflicts
from my_api_request_app import get_status_description

@csrf_exempt
def test_link(request):
    if request.method == 'POST':
        link = request.POST.get('link')
        environment = request.POST.get('env')

        if not link:
            return HttpResponse("Link is required", status=400)

        try:
            response = requests.get(link)
            status_code = response.status_code
            status_description = get_status_description(status_code)
            is_working = status_code == 200
        except requests.exceptions.RequestException as e:
            status_code = 500
            status_description = "Error occurred: {}".format(str(e))
            is_working = False

        created_at = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        username = request.user.username if request.user.is_authenticated else "Anonymous User"

        # Log the error here if needed

        return render(request, 'status_report.html',
                      {'link': link, 'status_code': status_code, 'status_description': status_description,
                       'is_working': is_working, 'created_at': created_at, 'username': username, 'environment': environment})
    else:
        return HttpResponse("Method not allowed", status=405)
