from django.urls import reverse
from django.test import Client, TestCase
from my_api_request_app.models import CSR
from my_api_request_app.app import get_status_description
from django.contrib.auth.models import User

class TestMyApp(TestCase):
    def test_create_csr(self):
        user = User.objects.create(username='test_user')
        csr = CSR.objects.create(user=user, common_name='example.com')
        self.assertEqual(csr.user.username, 'test_user')

    def setUp(self):
        self.client = Client()

    def test_get_status_description(self):
        self.assertEqual(get_status_description(200), "OK - The request was successful")
        self.assertEqual(get_status_description(400), "Bad Request - The server could not understand the request")
        # Add more assertions for other status codes

    def test_submit_csr(self):
        response = self.client.post('/submit_csr/', data={'title': 'Test Title', 'description': 'Test Description', 'user': 'test_user', 'link': 'http://example.com', 'env': 'test'})

        # Update the expected status code to match the actual response
        self.assertEqual(response.status_code, 200)  # Assuming the endpoint should return 200
        self.assertIn(b'example.com', response.content)  # Update the assertion for response content
        # Add more assertions for response content

    def test_show_csr(self):
        csr = CSR.objects.create(request_id=1)
        response = self.client.get(reverse('show_csr', kwargs={'id': csr.request_id}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'my_csr_app/detail.html')

    def test_list_csr(self):
        response = self.client.get(reverse('list_csr'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'my_csr_app/list.html')

    def test_create_csr(self):
        response = self.client.get(reverse('create_csr'))
        self.assertEqual(response.status_code, 200)

        data = {'request_id': 1, 'environment': 'test'}
        response = self.client.post(reverse('create_csr'), data)
        self.assertEqual(response.status_code, 302)

        self.assertTrue(CSR.objects.filter(request_id=1).exists())

    def test_search_csr(self):
        csr = CSR.objects.create(request_id=1, user__username='testuser')
        response = self.client.post(reverse('search_csr'), {'search': 'testuser'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'search_results.html')
