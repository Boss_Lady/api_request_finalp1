from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CSR',
            fields=[
                ('request_id', models.BigAutoField(primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
                ('description', models.TextField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('link', models.URLField()),
                ('environment', models.CharField(choices=[('prod', 'Prod'), ('dev', 'Dev'), ('test', 'Test')], max_length=10)),
            ],
            options={
                'managed': True,  # Changed from False to True to allow migration to manage the model
            },
        ),
    ]
