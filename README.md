# Viewing `my_api_request_app` Django Project in a Virtual Environment

This guide will help you set up and view the `my_api_request_app` Django project in a virtual environment on both Windows and Mac operating systems.

## Prerequisites

- Python installed on your system
- Virtual environment tool (virtualenv or venv)
- Basic understanding of using the command line

## Setup

1. Clone the `my_api_request_app` Django project repository to your local machine.
2. Navigate to the project directory using the command line.

## Windows

### Setting up the Virtual Environment

1. Open a command prompt.
2. Create a new virtual environment by running the following command: python -m venv myenv (.api_request)
3. Activate the virtual environment by running: .\.(api_request) myenv\Scripts\activate.ps1


### Installing Dependencies

1. Once the virtual environment is activated, install the project dependencies using: pip freeze > requirements.txt


### Running the Django Server

1. Navigate to the `my_api_request_app` Django project directory where `manage.py` is located.
2. Run the following command to start the Django development server: python manage.py runserver

3. Open a web browser and visit `http://127.0.0.1:8000` to view your `my_api_request_app`.

## Mac

### Setting up the Virtual Environment

1. Open a terminal.
2. Create a new virtual environment by running the following command: python3 -m venv myenv (.api_request)
3. Activate the virtual environment by running: source (api_request) myenv\bin\activate


### Installing Dependencies

1. Once the virtual environment is activated, install the project dependencies using: pip freeze > requirements.txt

### Running the Django Server

1. Navigate to the `my_api_request_app` Django project directory where `manage.py` is located.
2. Run the following command to start the Django development server: python manage.py py runserver

3. Open a web browser and visit `http://127.0.0.1:8000` to view your `my_api_request_app`.

## Additional Notes

- Remember to deactivate the virtual environment when you are done by running `deactivate` in the command line.
- Make sure to update the `requirements.txt` file with any new dependencies added to the project.

That's it! You should now be able to view your `my_api_request_app` Django project in a virtual environment on both Windows and Mac operating systems.

   
   
      
            